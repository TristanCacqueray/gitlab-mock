# Nix expressions to work on gitlab-mock
{ withHoogle ? false, static ? false }:
let
  # pin the upstream nixpkgs
  nixpkgsPath = fetchTarball {
    url =
      "https://github.com/NixOS/nixpkgs/archive/9fc2cddf24ad1819f17174cbae47789294ea6dc4.tar.gz";
    sha256 = "058l6ry119mkg7pwmm7z4rl1721w0zigklskq48xb5lmgig4l332";
  };
  nixpkgsSrc = (import nixpkgsPath);

  # use gitignore.nix to filter files from the src and avoid un-necessary rebuild
  gitignoreSrc = pkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore.nix";
    # put the latest commit sha of gitignore Nix library here:
    rev = "211907489e9f198594c0eb0ca9256a1949c9d412";
    # use what nix suggests in the mismatch message here:
    sha256 = "sha256-qHu3uZ/o9jBHiA3MEKHJ06k7w4heOhA+4HCSIvflRxo=";
  };
  inherit (import gitignoreSrc { inherit (pkgs) lib; }) gitignoreSource;

  # update haskell dependencies
  compilerVersion = "8104";
  compiler = "ghc" + compilerVersion;
  haskellOverrides = {
    overrides = hpFinal: hpPrev: {
      # use a more recent version that is not broken in nix
      fakedata = let
        fakedataSrc = builtins.fetchGit {
          url = "https://github.com/fakedata-haskell/fakedata.git";
          rev = "8ede9a9dbf1325df0295883eab59e74108729a28";
          submodules = true;
        };
      in pkgs.haskell.lib.dontCheck
      (hpPrev.callCabal2nix "fakedata" fakedataSrc { });

      gitlab-haskell = pkgs.haskell.lib.overrideCabal hpPrev.gitlab-haskell {
        version = "0.3.2.0";
        sha256 = "sha256:1ibsb4wyavyb6s64czy1pdbcskfwn96wcky4vlpr0r1f8gjpym5s";
      };

      autodocodec = let
        autodocodecSrc = builtins.fetchGit {
          url = "https://github.com/NorfairKing/autodocodec.git";
          rev = "6e3809376f56eff1da475a2dbf50bca16cbe1d77";
        };
      in pkgs.haskell.lib.dontCheck
      (hpPrev.callCabal2nix "autodocodec" "${autodocodecSrc}/autodocodec" { });

      gitlab-mock = (hpPrev.callCabal2nix "gitlab-mock" (gitignoreSource ./.)
        { }).overrideAttrs (_: { LANG = "en_US.UTF-8"; });
    };
  };

  pkgsBase = nixpkgsSrc { system = "x86_64-linux"; };

  pkgs = (if static then pkgsBase.pkgsMusl else pkgsBase);

  # Borrowed from https://github.com/dhall-lang/dhall-haskell/blob/master/nix/shared.nix
  statify = (if static then
    drv:
    pkgs.haskell.lib.appendConfigureFlags
    (pkgs.haskell.lib.disableLibraryProfiling
      (pkgs.haskell.lib.disableSharedExecutables
        (pkgs.haskell.lib.justStaticExecutables
          (pkgs.haskell.lib.dontCheck drv)))) [
            "--enable-executable-static"
            "--extra-lib-dirs=${
              pkgs.ncurses.override {
                enableStatic = true;
                enableShared = true;
              }
            }/lib"
            "--extra-lib-dirs=${pkgs.gmp6.override { withStatic = true; }}/lib"
            "--extra-lib-dirs=${pkgs.zlib.static}/lib"
            "--extra-lib-dirs=${
              pkgs.pkgsMusl.libsodium.overrideAttrs
              (old: { dontDisableStatic = true; })
            }/lib"
            "--extra-lib-dirs=${
              pkgs.libffi.overrideAttrs (old: { dontDisableStatic = true; })
            }/lib"
          ]
  else
    drv: drv);

  hsPkgs = pkgs.haskell.packages.${compiler}.override haskellOverrides;

in {
  gitlab-mock = statify hsPkgs.gitlab-mock;

  shell = hsPkgs.shellFor {
    packages = p: [ p.gitlab-mock ];
    buildInputs = with hsPkgs; [ cabal-install ghcid doctest ];
    withHoogle = withHoogle;
  };
}
