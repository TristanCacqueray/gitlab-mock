{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module GitLab.SystemHooks.Types.Mock where

import Autodocodec
import Data.Aeson (ToJSON)
import Faker (Fake)
import qualified Faker.TvShow.Futurama
import qualified Faker.TvShow.SiliconValley
import GHC.Generics (Generic)
import GitLab.SystemHooks.Types

-- $setup
-- >>> import Faker (generate)
-- >>> import Data.Aeson (encode)

----------------------
-- CommitAuthorEvent
----------------------
newtype CommitAuthorEventMock = CommitAuthorEventMock CommitAuthorEvent
  deriving stock (Show, Eq, Generic)
  deriving (ToJSON) via (Autodocodec CommitAuthorEvent)

instance HasCodec CommitAuthorEvent where
  codec =
    object "CommitAuthorEvent" $
      CommitAuthorEvent
        <$> requiredField "name" "the author name" .= commitAuthorEvent_name
        <*> requiredField "email" "the author email" .= commitAuthorEvent_email

-- | Generate a CommitAuthorEvent
--
-- >>> generate fakeCommitAuthorEvent
-- CommitAuthorEvent {commitAuthorEvent_name = "Zapp Brannigan", commitAuthorEvent_email = "monica@raviga.test"}
-- >>> encode . CommitAuthorEventMock <$> generate fakeCommitAuthorEvent
-- "{\"name\":\"Zapp Brannigan\",\"email\":\"monica@raviga.test\"}"
fakeCommitAuthorEvent :: Fake CommitAuthorEvent
fakeCommitAuthorEvent =
  CommitAuthorEvent
    <$> Faker.TvShow.Futurama.characters
    <*> Faker.TvShow.SiliconValley.email
